CREATE TABLE book (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    description VARCHAR(255),
    available BOOLEAN DEFAULT TRUE
);

INSERT INTO book (title, description, available) VALUES 
('Harry Potter and the Philosopher''s Stone', 'The first book in the Harry Potter series', true),
('To Kill a Mockingbird', 'A novel by Harper Lee', true),
('1984', 'A dystopian social science fiction novel by George Orwell', true),
('The Great Gatsby', 'A novel by F. Scott Fitzgerald', true),
('Pride and Prejudice', 'A romantic novel by Jane Austen', true);
