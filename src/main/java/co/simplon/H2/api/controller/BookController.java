package co.simplon.H2.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.H2.api.model.Book;
import co.simplon.H2.api.service.BookService;

@RestController
public class BookController {
    
    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/test")
    public String getTestMessage() {
        return "OK";
    }

    @GetMapping("/books")
    public Iterable<Book> getBooks() {
        return bookService.getBooks();
    }

    @PostMapping("/books")
    public Book createBook(@RequestBody Book book) {
        return bookService.createBook(book);
    }
}
