package co.simplon.H2.api.service;

import org.springframework.stereotype.Service;

import co.simplon.H2.api.model.Book;
import co.simplon.H2.api.repository.BookRepository;
import lombok.Data;

@Data
@Service
public class BookService {
    
    private BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Iterable<Book> getBooks() {
        return bookRepository.findAll();
    }

    public Book createBook(Book book) {
        return bookRepository.save(book);
    }
}
