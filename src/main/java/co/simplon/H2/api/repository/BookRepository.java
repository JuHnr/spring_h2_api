package co.simplon.H2.api.repository;

import org.springframework.data.repository.CrudRepository;

import co.simplon.H2.api.model.Book;

public interface BookRepository extends CrudRepository<Book, Long>{
    
}
